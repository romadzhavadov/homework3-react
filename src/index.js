import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
// через  команду:  npm install react-error-boundary
import { ErrorBoundary } from "react-error-boundary";
// import ErrorBoundary from './components/ErrorBoundary';
import SomethingWentWrong from './components/SomethingWentWrong/SomethingWentWrong';
import { BrowserRouter } from 'react-router-dom';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <ErrorBoundary fallback={<SomethingWentWrong />}>
      <BrowserRouter>
        <App />
        </BrowserRouter>
    </ErrorBoundary>


    // <ErrorBoundary >  вариант 2 
    //     <App />
    // </ErrorBoundary>
);



