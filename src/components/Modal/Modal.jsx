import React from "react";
import styles from './Modal.module.scss'
import PropTypes from "prop-types";


const Modal = (props) => {

    const { header, isCloseButton, text, actions, closefunction} = props;

    return(
        <div className={styles.wrapper}>
            <div className={styles.modalBackground} onClick={closefunction}></div>
            <div className={styles.modal}>
                <div className={styles.modalHeaderContainer}>
                    <h2 className={styles.modalTitle}>{header}</h2>
                   {isCloseButton ? <span className={styles.closeModalButton} onClick={closefunction}>&times;</span> : null} 
                </div>
                <p className={styles.modalContent}>{text}</p>
                {actions}
            </div>
        </div>

    )
}

Modal.propTypes = {
    header: PropTypes.string,
    isCloseButton: PropTypes.func,
    text: PropTypes.string,
    closefunction: PropTypes.func,
    actions: PropTypes.object.isRequired,
}

Modal.defaultProps = {
    header: 'text',
    isCloseButton: () => {},
    text: 'text',
    closefunction: () => {},
}

export default Modal;